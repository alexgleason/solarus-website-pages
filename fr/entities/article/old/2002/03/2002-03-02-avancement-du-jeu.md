<p>Le jeu avance à très grands pas vers la fin ! Je viens juste d'envoyer un guide complet à Christopho pour le niveau 8 qui lui posait énormément de problème, tout y est expliqué. Avec quelques modifications, ce guide deviendra une soluce pour les joueurs, elle sera consultable sur le site...</p>
<p>Pour ce qui est du contenu du jeu... Vous trouverez :</p>
<ul>
  <li>Le jeu (quand même) --- environ 5 Mo</li>
  <li>Une notice (en fichier HLP) --- environ 800 Ko</li>
  <li>Les musiques MIDI originales (qu'on peut aussi retrouver sur le site) ---
    environ 300 Ko</li>
  <li>Un dossier spécial sur les techniques de développement (HTML) ---
    environ 500 Ko</li>
  <li>Un petit fichier lisez-moi obligatoire --- environ 5 Ko</li>
</ul>
<p>TOTAL éstimé : 6600 Ko soit 6,6 Mo</p>
<p>Et oui car si vous souhaitez télécharger juste le jeu, vous avez quand
même un fichier lisez-moi qui vous donnera les indications de bases du jeu. Les
tailles des fichiers sont données arbitrairement, il est possible que cela
change, vous pourrez télécharger chaque composant séparemment si vous le
voulez. Ou bien télécharger trois types de packs :</p>
<ul>
  <li>FULL-PACK : tout les composants y sont --- 6,6 Mo</li>
  <li>GAME-PACK : jeu + notice --- 5,8 Mo</li>
  <li>LIGHT-PACK : jeu + lisez-moi --- 5 Mo</li>
</ul>
<p>Les informations seront redonnées lors de la sortie du jeu !</p>
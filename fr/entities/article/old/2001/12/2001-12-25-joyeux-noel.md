<p>Voici votre cadeau de noël : six screenshots du niveau 7 !</p>

<table border="0" cellpadding="15" cellspacing="0" align="center">
<tr align="center">
<td><a href="/images/solarus-ecran51.png" target="_blank"><img src="/images/solarus-ecran51.png" width=160 height=120 border=0></a></td>
<td><a href="/images/solarus-ecran52.png" target="_blank"><img src="/images/solarus-ecran52.png" width=160 height=120 border=0></a></td>
</tr>
<tr align="center">
<td><a href="/images/solarus-ecran53.png" target="_blank"><img src="/images/solarus-ecran53.png" width=160 height=120 border=0></a></td>
<td><a href="/images/solarus-ecran54.png" target="_blank"><img src="/images/solarus-ecran54.png" width=160 height=120 border=0></a></td>
</tr>
<tr align="center">
<td><a href="/images/solarus-ecran55.png" target="_blank"><img src="/images/solarus-ecran55.png" width=160 height=120 border=0></a></td>
<td><a href="/images/solarus-ecran56.png" target="_blank"><img src="/images/solarus-ecran56.png" width=160 height=120 border=0></a></td>
</tr>
</table>

<p>Vous l'avez compris, le niveau 7 est un donjon glacial.</p>

<p>Composé de trois grands étages, ce donjon apporte quelques nouveautés au jeu. Les énigmes seront assez novatrices par rapport aux autres donjons. Certaines sont inspirées des derniers donjons de Zelda 3...</p>

<p>Le donjon est quasiment terminé : il ne reste plus que le mini-boss et le Boss à programmer. Nous vous tenons au courant !</p>
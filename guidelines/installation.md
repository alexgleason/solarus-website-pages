# Installing the website

## Installing a local server

We strongly recommend to test the modifications you make in an actual environment. Modifying files without seeing the result is bad practice. For this, you will need to setup a local web server.

### Windows

You may use WAMP, which stands for "Windows Apache MySQL PHP". [Download it](http://www.wampserver.com/) and install it. It will, by default, be installed into `C:\wamp`.

Start WAMP from the Start menu, and when it's started, you can access your website in your browser, by typing `localhost`in the address bar.

### MacOS

You may use MAMP, which stands for "Mac Apache MySQL PHP". [Download it](https://www.mamp.info) and install it.

MAMP allows you to modify the folder that will be used by your server. We recommend not using the default one, which is in `applications` but rather setting it to where you store your development projects (`~/Documents/Development/` for instance). You can modify this in the menu **MAMP > Preferences > Web Server > Documents Root**.

### Linux

Coming soon.

## Installing Git

You need to install [Git](https://git-scm.com/) to contribute to the website. Git is a free and open source distributed version control system.

Follow the installation instructions for the platform you are working on.

Git is originally intended to be used with a terminal, but some graphical apps exist. However, the terminal allows for more control, and we recommend using it.

**NB:** On Windows, the native console is not well suited for using Git. So the Git installation also provides Git Bash, a console that looks like a Unix terminal, just like on Linux and MacOS. Use this one.

## Downloading the website

Open your terminal, and go into the folder where your server will look up the files.

```bash
cd path/to/your/server
```
**NB:** On Windows, it's in `C:/wamp/www/`.

There, we will download first the [website engine](https://gitlab.com/solarus-games/kokori). First, fork the project `kokori`: it means you create your own copy, since you cannot modify directly ours.

Then, clone it to your computer. It will download a local copy to your computer, in a folder named `kokori`.

```bash
git clone https://gitlab.com/your-name/kokori
```

The engine serves pages that are located in its `public/data` folder. Currently, at this step, the `data` folder is tempty. This is where we will download the actual pages.

Then, fork [the pages](https://gitlab.com/solarus-games/solarus-website-pages), i.e. the repository `solarus-website-pages`, and clone it into a folder named `data`:

```bash
git clone git@gitlab.com:your-name/solarus-website-pages.git data
```

Now, you should have both the engine and the pages on your computer.

## Testing the website

Ensure that your server (WAMP, MAMP or any other way), and open a web browser. Type `localhost`in the address bar (`localhost:8888` for a non-Windows OS). You should see the website in your browser.

Modifying a file needs the browser to be refreshed to see the changement.

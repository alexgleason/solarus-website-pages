[container]

# {title}

[alert type="secondary" title="Heye!" icon="exclamation-triangle"]
This page is currently under construction.
[/alert]

## Video tutorials

Christopho made video tutorials on Youtube about the basics of Solarus. They cover the basics of the creation of an ARPG with the engine and the game editor. You can also go to the [complete playlist](https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufxwkj7IlfURcvCxaDCSecJY).

1. [Create a quest](https://www.youtube.com/watch?v=8StwujI-Hbg)
2. [Zelda resource pack](https://www.youtube.com/watch?v=QGrqnz78IX8)
3. [The map editor](https://www.youtube.com/watch?v=3rpmpdr-_F0)
4. [Grounds](https://www.youtube.com/watch?v=mQsdeDs0kf8)
5. [The quest tree](https://www.youtube.com/watch?v=IhLfQ_YzEKQ)
6. [Quest properties](https://www.youtube.com/watch?v=8WhpIbO2SG0)
7. [Getting started with Lua scripting](https://www.youtube.com/watch?v=WpSp7_F0SIM)
8. [Treasures](https://www.youtube.com/watch?v=iG0YRerYphg&t=0s)
9. [Save the game](https://www.youtube.com/watch?v=fPdFOG24dE4&t=0s)
10. [Teletransporters](https://www.youtube.com/watch?v=3Z7Hol4j0II)
11. [How to scroll between maps of different sizes](https://www.youtube.com/watch?v=5SN2RKV09ec)
12. [Stairs and holes](https://www.youtube.com/watch?v=ZU60nuP2YAk&t=0s)
13. [Organize your code in separate files with require()](https://www.youtube.com/watch?v=CUcfPYMlVs8)
14. [How to create a tileset](https://www.youtube.com/watch?v=bC0IScIohjA)
15. [Map scripts](https://www.youtube.com/watch?v=4wPnAM6eoTo)
16. [How to create a sprite](https://www.youtube.com/watch?v=0ajYOpUoFR4)
17. [How to display an image](https://www.youtube.com/watch?v=5Jsh43NzHdw)
18. [Dialogs with a non-playing character](https://www.youtube.com/watch?v=KyF4LB1YOSY)
19. [Generalized NPCs](https://www.youtube.com/watch?v=3aLyAJfhHzc)
20. [Movements](https://www.youtube.com/watch?v=jAsqZ2CjXkI)
21. [Jumpers](https://www.youtube.com/watch?v=9_AWDGgfEx0)
22. [Platforms](https://www.youtube.com/watch?v=iRXMIY3ChaM)
23. [Switch that triggers a mechanism](https://www.youtube.com/watch?v=IFS3eeEVHWw)
24. [Dynamic Tiles](https://www.youtube.com/watch?v=M76i06qEX6A)
25. [Timers](https://www.youtube.com/watch?v=RIjs7cHZe-0)
26. [Enemies](https://www.youtube.com/watch?v=KZoQxbrZrJ4)
27. [Blocks](https://www.youtube.com/watch?v=pOm_I2kG6vM)
28. [Invisible walls](https://www.youtube.com/watch?v=IHuHkcQ2jrs)
29. [Separators](https://www.youtube.com/watch?v=__Ct85wNISo)
30. [Sensors](https://www.youtube.com/watch?v=XJEEdqijh60)
31. [Destructible objects](https://www.youtube.com/watch?v=rCGwzmdiKk8)
32. [Doors](https://www.youtube.com/watch?v=mYjctnDqsrU)
33. [The HUD](https://www.youtube.com/watch?v=RvV2rU75WmA)
34. [Rupees](https://www.youtube.com/watch?v=zH2nzQemzRs)
35. [Shops](https://www.youtube.com/watch?v=MdPaOsu-62A)
36. [Let's swim](https://www.youtube.com/watch?v=aoMXvvMyk1k)

## Migration guide

If your game is still using an old Solarus version, you may want to migrate to a new one. We made a [migration guide](/en/development/tutorials/migration-guide).

[/container]

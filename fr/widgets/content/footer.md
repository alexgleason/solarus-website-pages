[container]
[row]
[column]

#### Informations légales

**© 2006-2019 Christopho, Solarus**</br>
Les logiciels Solarus sont sous licence GPL v3. Les ressources sont sous licence CC-BY-SA 4.0.
Le contenu de ce site est sous licence CC-BY-SA 4.0.
[align type="center"]
[![GPL v3 logo](images/gpl_v3_logo.png)](https://www.gnu.org/licenses/quick-guide-gplv3.html)
[space orientation="vertical" thickness="10"]
[![CC-BY-SA 4.0 logo](images/cc_logo.png)](https://creativecommons.org/licenses/by-sa/4.0)
[/align]

[/column]
[column]

#### À propos

Solarus est maintenu par une équipe de bénévoles sur leur temps libre.

* [link title="Foire Aux Questions" label="Questions fréquentes" url="/fr/about/faq" icon="question-circle" target=""]
* [link title="Équipe" label="Équipe" url="/fr/about/team" icon="users" target=""]
* [link title="Contact" label="Contact" url="/fr/about/contact" icon="envelope-open-text" target=""]

[/column]
[column]

#### Contribuer

Solarus est gratuit : votre aide ou support sera très apprécié.

* [link title="Comment contribuer" label="Comment contribuer" url="/fr/development/how-to-contribute" icon="hand-holding-heart" target=""]
* [link title="Faire un don" label="Faire un don" url="/fr/development/donation" icon="donate" target=""]

[/column]
[column]

#### Liens

Liens utiles pour rester informé sur le projet.

[link title="Gitlab" url="https://gitlab.com/solarus-games" icon="gitlab" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Twitter" url="https://www.twitter.com/SolarusGames" icon="twitter" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Facebook" url="https://www.facebook.com/solarusgames" icon="facebook" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Discord" url="https://discord.gg/PtwrEgZ" icon="discord" icon-category="fab" icon-size="2x" target=""]

[space thickness="20"]

[align type="center"]
![Solarus Team logo](images/solarus_team_logo.png)
[/align]

[/column]
[/row]

[row]
[column width="3"]
[space]
[/column]
[column width="6"]
[align type="center"]
[space orientation="vertical" thickness="10"]

Réalisé avec [Kokori](https://gitlab.com/solarus-games/kokori), notre propre moteur de site libre et open-source.

[/align]
[/column]
[column width="3"]
[space]
[/column]

[/row]

[/container]

[container]

# {title}

This page explains how to upgrade a Solarus quest to the latest Solarus version.

## Solarus version numbers

Since Solarus 1.0.0, versions of Solarus are numbered as follows: `x.y.z`, where

* `x` is the **major version**,
* `y` is the **minor version**,
* `z` is the **patch version**.

Patch versions contain only bug fixes. They never introduce incompatibilities, so when only the patch version changes, your quest continues to work.

Therefore, when we talk about compatibility, only the major and minor numbers are considered. In your quest properties file `quest.dat`, the value `solarus_version` indicates the format of your quest, with only the major and minor numbers. For example, if `solarus_version` is `1.5`, your quest is compatible with Solarus `1.5.*`, where `*` is any patch version number.

To make your quest compatible with the latest version of Solarus, there are two steps:

1. **Upgrading data files**: when your quest is obsolete, the editor shows a dialog that lets you automatically convert it to the latest version.
2. **Upgrading scripts**: Lua scripts are programs, so there is no way to convert them automatically when something changes in the [Solarus Lua API](http://www.solarus-games.org/doc/latest). The goal of this migration guide is to help you doing the upgrade.

## Guides

Each migration has its specificities. Be sure to not miss anything.

* [From Solarus 1.0 to Solarus 1.1](/en/development/tutorials/migration-guide/from-v1.0-to-v1.1)
* [From Solarus 1.1 to Solarus 1.2](/en/development/tutorials/migration-guide/from-v1.1-to-v1.2)
* [From Solarus 1.2 to Solarus 1.3](/en/development/tutorials/migration-guide/from-v1.2-to-v1.3)
* [From Solarus 1.3 to Solarus 1.4](/en/development/tutorials/migration-guide/from-v1.3-to-v1.4)
* [From Solarus 1.4 to Solarus 1.5](/en/development/tutorials/migration-guide/from-v1.4-to-v1.5)

[/container]

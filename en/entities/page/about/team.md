[container]

# {title}

## History

Everything began back in 2002, when Christopho, the creator and main developer of Solarus, created a website for its first fangame project: Zelda-Solarus. It was a way to promote the first version of Mystery of Solarus, a game developed with RPG Maker.

As time passing by, the website and its forum grew, and began to be a place not only for French Zelda-like game makers, but also Zelda-lovers since it was a general Zelda website as well with mangas, walkthroughs, reviews, resources and news.

In 2006, Christopho decided to rework his first game in C++, and Solarus was born. Later, it was decided that the engine needed a website for itself. Both websites eventually merged in 2019, to become the Solarus-Games website you know today.

The project continues to grow up and is becoming a general Action-RPG game engine. More people are joining the project, and we are receiving more and more contributions.

## Members

Solarus is developed by a team of enthusiasts on their free time, as a hobby. Here is a list of the main contributors. Use this list if you need information about a specific part of the project.

[row]
[column]
[user-presentation name="Christopho" description="Founder and lead developer of Solarus" link-label="@ChristophoZS" link-title="Christopho" link-url="#" link-target="_parent" image="/data/assets/images/team/christopho.jpg"]
[/column]
[column]
[user-presentation name="std::gregwar" description="Developer" link-label="@stdgregwar" link-title="std::gregwar" link-url="https://gitlab.com/stdgregwar" link-target="_parent" image="/data/assets/images/team/stdgregwar.jpg"]
[/column]
[column]
[user-presentation name="Binbin" description="Web developer, game developer" link-label="@zelda_force" link-title="Binbin" link-url="https://twitter.com/zelda_force" link-target="_parent" image="/data/assets/images/team/binbin.jpg"]
[/column]
[/row]

[row]
[column]
[user-presentation name="Olivier Cléro" description="Artist, game developer" link-label="@olivclr" link-title="Olivier Cléro" link-url="https://twitter.com/olivclr" link-target="_parent" image="/data/assets/images/team/oclero.jpg"]
[/column]
[column]
[user-presentation name="Maxs" description="Quest Editor developer" link-label="@Maxs1789" link-title="Maxs" link-url="https://twitter.com/Maxs1789" link-target="_parent" image="/data/assets/images/team/maxs.jpg"]
[/column]
[column]
[user-presentation name="Renkineko" description="Game developer, writer" link-label="@renkineko" link-title="Renkineko" link-url="https://twitter.com/renkineko" link-target="_parent" image="/data/assets/images/team/renkineko.jpg"]
[/column]
[/row]

[row]
[column]
[user-presentation name="Diarandor" description="Game developer, artist" link-label="@Diarandor" link-title="Diarandor" link-url="https://twitter.com/Diarandor" link-target="_parent" image="/data/assets/images/team/diarandor.jpg"]
[/column]
[column]
[user-presentation name="Metallizer" description="Co-founder, game developer" link-label="@is06" link-title="is06" link-url="https://twitter.com/is06" link-target="_parent" image="/data/assets/images/team/metallizer.jpg"]
[/column]
[column]
[user-presentation name="Max Mraz" description="Artist, game developer" link-label="@11Mraz" link-title="11Mraz" link-url="https://twitter.com/11Mraz" link-target="_parent" image="/data/assets/images/team/max.jpg"]
[/column]
[/row]

[/container]

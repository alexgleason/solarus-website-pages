### Présentation

*The Legend of Zelda: Onilink Begins SE* est un projet de remake fait avec Solarus du second jeu de Vincent Jouillat, d'où le suffixe *SE* qui signifie *Solarus Edition*. Le jeu d'origine a été originellement publié le 12 Août 2007. Le jeu est refait entièrement avec Solarus pour profiter des meilleurs capacités du moteur.

### Synopsis

Abattu par un terrible maléfice depuis sa récente victoire sur Ganon, Link se transforme, jour après jour, en puissante créature à l'instinct destructeur nommée Oni-Link. Banni d'Hyrule, le jeune hylien demande à la princesse Zelda de l'aide. Elle lui montre alors ce qui semble être son dernier espoir : un portail vers un monde secret.

![Link](artworks/artwork_link.png "Link")

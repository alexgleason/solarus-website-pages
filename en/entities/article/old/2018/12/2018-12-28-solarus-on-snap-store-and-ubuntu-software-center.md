Solarus is finally available on <strong>Snap Store</strong> and <strong>Ubuntu Software Center</strong>. Installing Solarus has never been easier! Thanks to Alex Gleason.
<h3>On Ubuntu</h3>
<img class="aligncenter wp-image-1944" src="/images/Ubuntu_Software_Center_icon_v3-01.png" alt="" width="64" height="64" />

Search for <strong><em>Solarus</em> </strong>in the Software Center and install it from here.
<a href="/images/Screenshot-from-2018-12-28-10-15-37.png"><img class="aligncenter wp-image-1948 size-full" src="/images/Screenshot-from-2018-12-28-10-15-37.png" alt="" width="731" height="552" /></a>

Click on <strong>Install</strong>.

<a href="/images/Screenshot-from-2018-12-28-10-16-13.png"><img class="aligncenter wp-image-1949 size-full" src="/images/Screenshot-from-2018-12-28-10-16-13.png" alt="" width="963" height="1063" /></a>

&nbsp;
<h3>On other Linux distros</h3>
<a href="https://snapcraft.io/solarus"><img class="aligncenter" src="/images/snap-store-black.svg" alt="Get it from the Snap Store" /></a>

First, ensure you have <strong>snapd</strong> installed. If not, <a href="https://docs.snapcraft.io/installing-snapd/6735">install it with your package manager</a>. It should be something like:
<pre>sudo apt install snapd</pre>
Then, install <strong>Solarus</strong>.
<pre>sudo snap install solarus</pre>
<a href="http://www.solarus-games.org/wp-content/uploads/2018/12/snapstore_page-1.png"><img class="aligncenter wp-image-1947 size-medium" src="/images/snapstore_page-1-300x269.png" alt="" width="300" height="269" /></a>
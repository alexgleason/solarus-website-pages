Bonsoir à tous,

Pour pouvoir déposer Zelda Mystery of Solarus DX au pied du sapin, rien de telle qu'une boîte officielle !

(Cliquez sur les images pour agrandir)

[center][url=http://www.zelda-solarus.com/images/zsdx/boite_avant.jpg][img]http://www.zelda-solarus.com/images/zsdx/boite_avant_400.jpg[/img][/url]
[url=http://www.zelda-solarus.com/images/zsdx/boite_arriere.jpg][img]http://www.zelda-solarus.com/images/zsdx/boite_arriere_400.jpg[/img][/url]
[/center]

La boîte a été conçue et réalisée par Neovyse rien que pour vous !
Vous pouvez la télécharger, la découper et la monter vous-même en cliquant ci-dessous :

[center]
[url=http://www.zelda-solarus.com/zsdx/boite.pdf][img]http://www.zelda-solarus.com/images/zsdx/boite_a_monter.jpg[/img][/url]
[/center]

Un grand bravo à Neovyse pour ce travail, ses artworks et autres ?uvres Zelda-esques :)

Enjoy ^_^
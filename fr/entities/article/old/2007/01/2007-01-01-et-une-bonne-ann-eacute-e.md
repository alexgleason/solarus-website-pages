[b][center]Une bonne année et une bonne santé pour tous ![/center][/b]

Au nom de toute l'équipe je vous souhaite de passer une meilleure année que la précédente, tout plein de bonnes choses pour vous.
Mais n'oubliez pas que la chance ne s'acquiert pas forcément, on peut aussi la fabriquer.
Restez-vous même et surtout restez Zelda !


Enfin, merci à tous de rester fidèle à Solarus !
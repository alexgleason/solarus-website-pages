[container]

# {title}

[row]
[column]

[button-highlight type="primary" icon="question" url="about/faq" label="Foire Aux Questions"]

[/column]
[column]

[button-highlight type="primary" icon="team" url="about/team" label="Équipe"]

[/column]
[column]

[button-highlight type="primary" icon="mail" url="about/contact" label="Contact"]

[/column]
[/row]

[/container]
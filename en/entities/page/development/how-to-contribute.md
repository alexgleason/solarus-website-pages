[container]

[row]
[column width="8"]

# {title}

So you want to contribute to Solarus? All the community is thankful and any help, even very small, is welcome. Come talk with us on [Discord](https://discord.gg/yYHjJHt) to inform us about what you think you can bring to the project.

## Development

All of the Solarus projects source codes are on Gitlab. The engine is made in C++. It is recommended to be proficient in this language and to master video game programming patterns before being able to contribute. The Quest Editor and the Launcher are made in C++ with Qt.

* [Solarus projects on Gitlab](https://gitlab.com/solarus-games)
* [C++ Engine Code documention](http://www.solarus-games.org/developer_doc/latest)

If you want to **inform of a bug**, or if you have a **feature request**:

1. Look if anyone hasn't already created an issue in the issue tracker of the project.
2. Create your issue, and be as precise as possible. Join screenshots if needed.

If you want to **add or modify the source code** of any project:

1. Clone the repository you want to contribute to.
2. Create your own branch and make your modifications in this branch.
3. Create a Merge Request.

## Tutorials for quest makers

So you know quite well how to make a Solarus quest? Why not help people to learn it too? Tutorials need to be written to help begineers and more advanced quest makers. We have a repository on Gitlab for the official tutorials. They are written in markdown, which is pretty easy to learn.

* [Solarus tutorials on Gitlab](https://gitlab.com/solarus-games/learn-solarus)

Follow the same steps than developers of you want to contribute to this project:

1. Clone the repository.
2. Create your own branch and make your modifications in this branch.
3. Create a Merge Request.

## Art

If you're feeling artistic, we need help in the pixel art department. Any tileset, font or sprite contributions are welcome! You may also want to improve the website design?

* [Solarus graphic design elements on Gitlab](https://gitlab.com/solarus-games/solarus-design)
* [Solarus free resource pack on Gitlab](https://gitlab.com/solarus-games/solarus-free-resource-pack)

## Translations

The Quest Editor and the Launcher need to be translated in all the existing languages! Feel free to add your own language. Since they're built with Qt, you should Qt built-in translation manager named Qt Linguist.

The games may have new translations too. We have an in-house translator tool integrated into Solarus Quest Editor.

* [Tutorial about how to translate a quest](tutorials)

## Donations

Solarus developers create this engine on their spare time. If you appreciate their work, your donations can help with the hosting fees and show that Solarus is appreciated.

* [Donation page](donation)

[/column]

[column width ="4"]
[summary level-min="2" level-max="3"]
[/column]

[/row]
[/container]
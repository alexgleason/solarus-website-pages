[container]
[row]
[column]

#### Legal Information

**© 2006-2019 Christopho, Solarus**<br/>
Solarus software is licensed under GPL v3.<br/>Included assets are licensed under CC-BY-SA 4.0.
The whole content of this website is licensed under CC-BY-SA 4.0.
[align type="center"]
[![GPL v3 logo](images/gpl_v3_logo.png)](https://www.gnu.org/licenses/quick-guide-gplv3.html)
[space orientation="vertical" thickness="10"]
[![CC-BY-SA 4.0 logo](images/cc_logo.png)](https://creativecommons.org/licenses/by-sa/4.0)
[/align]

[/column]
[column]

#### About

Solarus is made by a team of people on their free time.

* [link title="Frequently Asked Questions" label="Frequently Asked Questions" url="/en/about/faq" icon="question-circle" target=""]
* [link title="Team" label="Team" url="/en/about/team" icon="users" target=""]
* [link title="Contact" label="Contact" url="/en/about/contact" icon="envelope-open-text" target=""]

[/column]
[column]

#### Contribute

Solarus is free: your help or support is always appreciated.

* [link title="How to contribute" label="How to contribute" url="/en/development/how-to-contribute" icon="hand-holding-heart" target=""]
* [link title="Donate" label="Donate" url="/en/development/donation" icon="donate" target=""]

[/column]
[column]

#### Links

Useful links for people who want to keep being informed about the project.

[link title="Gitlab" url="https://gitlab.com/solarus-games" icon="gitlab" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Twitter" url="https://www.twitter.com/SolarusGames" icon="twitter" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Facebook" url="https://www.facebook.com/solarusgames" icon="facebook" icon-category="fab" icon-size="2x" target=""]

[space orientation="vertical" thickness="10"]

[link title="Discord" url="https://discord.gg/yYHjJHt" icon="discord" icon-category="fab" icon-size="2x" target=""]

[space thickness="20"]

[align type="center"]
![Solarus Team logo](images/solarus_team_logo.png)
[/align]

[/column]
[/row]

[row]
[column width="3"]
[space]
[/column]
[column width="6"]
[align type="center"]
[space orientation="vertical" thickness="10"]

Made with [Kokori](https://gitlab.com/solarus-games/kokori), our own free and open-source website engine.

[/align]
[/column]
[column width="3"]
[space]
[/column]

[/row]

[/container]

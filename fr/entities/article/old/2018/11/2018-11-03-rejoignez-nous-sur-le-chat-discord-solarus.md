Vous êtes présents sur Discord ? Si oui, vous serez ravis d'apprendre que Solarus a désormais son salon de discussion officiel sur Discord, le chat des gamers.

Rejoignez-nous pour parler de Zelda, de la création de jeux avec Solarus, de vos projets ou des nôtres, de Minecraft, des IRLs ou de ce que vous voulez qui n'a rien à voir !
<ul>
 	<li><a href="https://discord.gg/yYHjJHt">Salon de discussion Solarus sur Discord</a></li>
</ul>
Venez nombreux. On vous attend !
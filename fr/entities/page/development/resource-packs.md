[container]

# {title}

[row]
[column width="3"]
[/column]
[column width="9"]
[entity-search text-placeholder="Rechercher"]
[/column]
[/row]

[space]

[row]

<!--Filters-->
[column width="3"]
[entity-filter entity-type="resource-pack" filter="licenses" title="Licence"]
[/column]

<!--Games -->
[column width="9"]
[resource-pack-listing]
[/column]

[/row]

[/container]
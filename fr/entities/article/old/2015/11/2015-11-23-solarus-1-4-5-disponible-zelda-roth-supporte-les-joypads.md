Une nouvelle version de Solarus et de nos jeux vient de sortir ! Elle corrige essentiellement de nombreux petits bugs et améliore l'utilisation de l'éditeur de quêtes.

Une nouveauté qui était très attendue est que <a href="http://www.zelda-solarus.com/zs/article/zroth-presentation/">Zelda Return of the Hylian</a> (Solarus Edition) est maintenant jouable avec une manette ! Et vous avez la possibilité de configurer les touches :

<img class="aligncenter" src="/images/CSLQAbHWoAAy572.png:large" alt="" width="640" height="480" />

Par ailleurs, Zelda Mystery of Solarus DX est maintenant traduit en italien (bêta) grâce à Marco B. !

Télécharger <a href="http://www.solarus-games.org/downloads/solarus/win32/solarus-1.4.5-win32.zip">Solarus 1.4.5 + l'éditeur de quêtes</a> pour Windows
<ul>
	<li>Télécharger le <a href="http://www.solarus-games.org/downloads/solarus/solarus-1.4.5-src.tar.gz">code source</a></li>
	<li>Liste complète des <a href="http://www.solarus-games.org/2015/11/22/bugfix-release-1-4-5-joypad-supported-in-zelda-roth/">changements</a></li>
	<li><a href="http://www.solarus-games.org/">Blog de développement Solarus</a></li>
	<li><a href="https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufySXw9_E-hJzmzSh-PYCyG2">Tutoriels vidéo</a></li>
</ul>
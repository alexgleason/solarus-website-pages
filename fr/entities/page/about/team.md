[container]

# {title}

## Histoire

Tout a commencé il y a bien longtemps, en 2001 pour être exact, quand Christopho, le créateur et principal contributeur de Solarus, a lancé un site Internet pour son premier projet de jeu amateur : Zelda-Solarus. C'était un moyen de promouvoir la toute première version de *Mystery of Solarus*, un jeu réalisé avec RPG Maker.

Au fur et à mesure, le site et son forum ont grossi, et sont devenus un lieu attirant non seulement les développeurs de jeux Zelda-like, mais aussi les fans de Zelda, puisque le site était également pourvu de mangas, soluces, tests, ressources et actualités à propos de cette série de jeux vidéo.

Christopho décida en 2006 de refaire en C++ son premier jeu, et Solarus était né. Il fut par la suite décidé que le moteur méritait d'avoir son propre site Internet. Les deux sites ont longtemps coexisté, avant de fusionner en 2019 pour donner le site Solarus-Games que vous connaissez aujourd'hui.

Le projet prend aujourd'hui de l'ampleur et devient un moteur de jeu Action-RPG généraliste. De plus en plus de personnes ont rejoint la communauté, et nous avons la chance de recevoir de nombreuses contributions.

## Membres

Solarus est développé par une équipe de passionnés sur leur temps libre. Voici une liste des principaux contributeurs. Vous pouvez utiliser cette liste pour savoir qui contacter à propos d'une partie spécifique du projet.

[row]
[column]
[user-presentation name="Christopho" description="Fondateur et développeur principal" link-label="@ChristophoZS" link-title="Christopho" link-url="#" link-target="_parent" image="/data/assets/images/team/christopho.jpg"]
[/column]
[column]
[user-presentation name="std::gregwar" description="Développeur" link-label="@stdgregwar" link-title="std::gregwar" link-url="https://gitlab.com/stdgregwar" link-target="_parent" image="/data/assets/images/team/stdgregwar.jpg"]
[/column]
[column]
[user-presentation name="Binbin" description="Développeur web, développeur de jeux" link-label="@zelda_force" link-title="Binbin" link-url="https://twitter.com/zelda_force" link-target="_parent" image="/data/assets/images/team/binbin.jpg"]
[/column]
[/row]

[row]

[column]
[user-presentation name="Olivier Cléro" description="Artiste, développeur de jeux" link-label="@olivclr" link-title="Olivier Cléro" link-url="https://twitter.com/olivclr" link-target="_parent" image="/data/assets/images/team/oclero.jpg"]
[/column]
[column]
[user-presentation name="Maxs" description="Développeur de l'éditeur de quêtes" link-label="@Maxs1789" link-title="Maxs" link-url="https://twitter.com/Maxs1789" link-target="_parent" image="/data/assets/images/team/maxs.jpg"]
[/column]
[column]
[user-presentation name="Renkineko" description="Développeur de jeux, rédacteur" link-label="@renkineko" link-title="Renkineko" link-url="https://twitter.com/renkineko" link-target="_parent" image="/data/assets/images/team/renkineko.jpg"]
[/column]
[/row]

[row]
[column]
[user-presentation name="Diarandor" description="Développeur de jeux, artiste" link-label="@Diarandor" link-title="Diarandor" link-url="https://twitter.com/Diarandor" link-target="_parent" image="/data/assets/images/team/diarandor.jpg"]
[/column]
[column]
[user-presentation name="Metallizer" description="Co-fondateur, développeur de jeux" link-label="@is06" link-title="is06" link-url="https://twitter.com/is06" link-target="_parent" image="/data/assets/images/team/metallizer.jpg"]
[/column]
[column]
[user-presentation name="Max Mraz" description="Artiste, développeur de jeux" link-label="@11Mraz" link-title="11Mraz" link-url="https://twitter.com/11Mraz" link-target="_parent" image="/data/assets/images/team/max.jpg"]
[/column]
[/row]

[/container]

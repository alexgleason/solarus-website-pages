On se remet doucement de la sortie de Zelda XD2 ! Après un peu de repos bien mérité, l'activité sur Mercuris Chest reprend dans l'équipe.

<a href="../data/fr/entities/article/old/2017/04/images/park.png"><img class="aligncenter size-medium wp-image-38167" src="/images/park-300x225.png" alt="park" width="300" height="225" /></a>

Voici la première capture d'écran d'une nouvelle région en cours de création par l'incontournable Newlink : un grand parc où Link va pouvoir s'amuser mais aussi faire quelques mini-quêtes importantes pour la suite de son voyage
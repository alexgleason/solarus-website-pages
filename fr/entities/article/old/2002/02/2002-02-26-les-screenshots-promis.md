<p>Voici les screenshots réclamés par Netgamer sur le <a href="http://www.zelda-solarus.com/forum.php3">forum</a> ! Ils correspondent à la news "Les nouveautés récentes" du 21 février : nouveaux chipsets des maisons et du donjon 2, et affichage des Coeurs et des Rubis à l'écran. Dans la colonne de gauche : ce qu'il y avait avant. Dans la colonne de droite : ce qu'il y a actuellement.</p>

<table border="0" cellpadding="15" cellspacing="0" align="center">
<tr align="center">
<td><a href="/images/solarus-ecran58.png" target="_blank"><img src="/images/solarus-ecran58.png" width="160" border="0"></a></td>
<td><a href="/images/solarus-ecran61.png" target="_blank"><img src="/images/solarus-ecran61.png" width="160" border="0"></a></td>
</tr>
<tr>
<td><a href="/images/solarus-ecran59.png" target="_blank"><img src="/images/solarus-ecran59.png" width="160" border="0"></a></td>
<td><a href="/images/solarus-ecran57.png" target="_blank"><img src="/images/solarus-ecran57.png" width="160" border="0"></a></td>
</tr>
<tr>
<td><a href="/images/solarus-ecran15.png" target="_blank"><img src="/images/solarus-ecran15.png" width="160" border="0"></a></td>
<td><a href="/images/solarus-ecran60.png" target="_blank"><img src="/images/solarus-ecran60.png" width="160" border="0"></a></td>
</tr>
</table>
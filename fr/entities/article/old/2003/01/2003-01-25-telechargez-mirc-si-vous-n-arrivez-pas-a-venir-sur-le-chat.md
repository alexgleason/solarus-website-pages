<p>Depuis ce matin, tout le réseau Internet est en plein dérangements à la suite d'une attaque la nuit dernière. Asterochat, qui permet en temps normal d'accéder au <a href="http://connexion.asterochat.com/?id=">chat</a>, semble marcher difficilement. Si vous n'arrivez pas vous y connecter, téléchargez mIRC en <a href="http://webperso.easynet.fr/mirc/mirc603.exe">cliquant ici</a>. Le logiciel ne fait que 1.16 Mo donc les petites connexions n'auront pas de problèmes.</p>

<p><b>Une fois mIRC installé, lancez-le : une boîte de dialogue "mIRC Options" apparaît. Sélectionnez "EpiKnet" dans la liste "IRC Network" et tapez un pseudo dans "Nickname". Cliquez sur OK, et vous pourrez ensuite venir sur le chat en tapant ceci : /join #zeldasolarus.</b></p>

<p><b>Le concours commencera finalement vers 21 h 30</b> car beaucoup d'entre vous risquez d'arriver en retard à cause de ce contretemps.</p>

<p>Si vous n'arrivez pas ou si vous ne savez pas comment vous connecter, envoyez-moi un e-mail : <a href="http://www.zelda-solarus.com/christopho@zelda-solarus.net">christopho@zelda-solarus.net</a> et je vous répondrai rapidement.</p>

<p>Voilà, désolé pour cet incident, on espère que vous viendrez quand même nombreux pour participer au concours !</p>

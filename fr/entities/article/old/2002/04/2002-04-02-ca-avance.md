<p>Salut à tous !</p>

<p>Le neuvième et dernier donjon a énormément avancé ce week-end. La programmation est longue et loin d'être passionnante, mais c'est bientôt fini ! J'espère finir toute la programmation du donjon dès ce soir ou demain.</p>

<p>Ensuite il nous restera encore beaucoup de travail, avec notamment le Boss final à créer. Mais rassurez-vous, nous sommes dans les temps. Il y a quelques semaines je pensais à retarder la sortie du jeu, mais finalement nous avons accéléré la conception et la programmation, ce qui va vous permettre de jouer à Zelda : Mystery of Solarus dans trois petites semaines seulement.</p>

<p>Malgré ce calendrier serré, de nombreux tests seront effectués par plus de 10 personnes et nous espérons ainsi que les bugs importants seront tous dénichés avant le 26 avril.</p>

<p>Enfin, n'oubliez pas de continuer à <a href="http://www.puissance-zelda.com/top/in.php3?id=27" target="_blank">voter</a> au Top 50 des sites sur Zelda, pour que Zelda Solarus continue à monter. Si nous atteignons le podium avant le 26 avril, nous vous promettons une grosse surprise !</p>
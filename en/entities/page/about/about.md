[container]

# {title}

[row]
[column]

[button-highlight type="primary" icon="question" url="about/faq" label="Frequently Asked Questions"]

[/column]
[column]

[button-highlight type="primary" icon="team" url="about/team" label="Team"]

[/column]
[column]

[button-highlight type="primary" icon="mail" url="about/contact" label="Contact"]

[/column]
[/row]

[/container]
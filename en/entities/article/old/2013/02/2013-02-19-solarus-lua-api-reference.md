The new Lua scripting API discussed in my previous post is making good progress!

I have written the full documentation. The goal of the Solarus engine is to be a generic Zelda-like 2D game engine, so it needs a powerful scripting API, as well as good documentation to make it usable.
This brand new Lua API allows to customize everything, including the menus and the head-up display.  The documentation fully specifies every data type accessible from Lua with their methods and callbacks: map entities, menus, sprites, movements, savegame, etc.
<ul>
	<li><a title="Solarus 1.0.0 API reference" href="http://www.solarus-games.org/doc/1.0.0/lua_api.html">Solarus 1.0.0 Lua API reference</a></li>
	<li><a title="Documentation" href="http://www.solarus-games.org/solarus/documentation/">Main documentation page</a></li>
</ul>
If you are making a game with the Solarus engine, this Lua API reference will become your favorite website :)
The API is still experimental, but most of the work is done. As it is getting more and more stable every day, I now keep the online documentation always up-to-date with the <a title="Source code" href="http://www.solarus-games.org/solarus/source-code/">git branch</a> solarus-1.0.0 of the engine.
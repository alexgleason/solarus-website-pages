[container]

# {title}

[row]

[column]

Avant de nous contactez, cherchez s'il-vous-plait si votre question ne serait pas déjà répondue dans la [Foire Aux Questions](/about/faq) !

Le [Forum Solarus](http://forum.solarus-games.org/) et le [Tchat Solarus](https://discord.gg/PtwrEgZ ) sont les lieux de discussion recommandés. Vouz aurez probablement de meilleures réponses qu'ici car toute la communauté Solarus sera là pour vous répondre. Si vous voulez davantage d'informations, proposer une suggestion our une contribution, ce sont les bons endroits.

Cependant, vous pouvez aussi nous contacter directement ici si vous avez une question bien spécifique ou si vous désirer nous contacter en privé. N'oubliez pas d'entrer une adresse e-mail valide pour que nous puissions vous répondre !

[/column]
[column]
[form-contact subject-label="Objet" message-label="Message" name-label="Nom" email-label="Courriel" button-label="Envoyer" captcha-sitekey="6LfDXZUUAAAAAF90gLsyZSWDZwv605BU6c24XJMv" message-success="Le message a bien été envoyé. Nous vous répondrons dans les plus bref délais." message-error="Le message n'a pas pu être envoyé car certains champs sont invalides." to-name="Christopho" to-email="christopho@solarus-games.org"]

[/column]

[/row]

[/container]

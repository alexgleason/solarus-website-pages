<span class="author-a-wymz79zlmz73zz86z09z70zz72z22z122zm">Depuis plusieurs années, et au risque de sacrifier la fréquence des mises à jour, nous développions dans le plus grand secret un projet d'une ampleur inédite.</span>
<div class="ace-line primary-author-a-wymz79zlmz73zz86z09z70zz72z22z122zm"></div>
<div class="ace-line primary-author-a-wymz79zlmz73zz86z09z70zz72z22z122zm">Cette fois il ne s'agit pas d'un jeu, ni d'un moteur de jeu, ni d'un logiciel de création de jeux. Nous sommes allés plus loin.</div>
<div class="ace-line primary-author-a-wymz79zlmz73zz86z09z70zz72z22z122zm"></div>
<div class="ace-line primary-author-a-wymz79zlmz73zz86z09z70zz72z22z122zm">
<div id="magicdomid1915" class="ace-line primary-author-a-wymz79zlmz73zz86z09z70zz72z22z122zm"><span class="author-a-wymz79zlmz73zz86z09z70zz72z22z122zm">L'équipe est fière de vous dévoiler officiellement la Solatrix, une console portable révolutionnaire dédiée aux jeux utilisant le moteur Solarus !</span></div>
<div class="ace-line primary-author-a-wymz79zlmz73zz86z09z70zz72z22z122zm"></div>
<div class="ace-line primary-author-a-wymz79zlmz73zz86z09z70zz72z22z122zm"><img class=" size-medium wp-image-38030 aligncenter" src="/images/logo_solatrix-300x65.png" alt="logo_solatrix" width="300" height="65" /></div>
<p class="ace-line primary-author-a-wymz79zlmz73zz86z09z70zz72z22z122zm"></p>

<div id="magicdomid1747" class="ace-line primary-author-a-wymz79zlmz73zz86z09z70zz72z22z122zm"><span class="author-a-wymz79zlmz73zz86z09z70zz72z22z122zm">La Solatrix vous permettra de télécharger les jeux créés avec Solarus Quest Editor, aussi bien ceux de notre équipe que les jeux de la communauté. Il est prévu qu'elle soit disponible pour les fêtes, même s'il reste à déterminer de quelle année.</span></div>
</div>
<a href="../data/fr/entities/article/old/2016/04/images/console1.jpg"><img class="aligncenter wp-image-38026 size-medium" src="/images/console1-300x225.jpg" alt="La Solatrix" width="300" height="225" /></a>

Légère et maniable, la Solatrix est une console portable de nouvelle génération connectée, et dotée d'un écran 3,5 pouces rétroéclairé.
<div id="magicdomid1227" class="ace-line primary-author-a-wymz79zlmz73zz86z09z70zz72z22z122zm"><span class="author-a-wymz79zlmz73zz86z09z70zz72z22z122zm">Pour les créateurs de jeux, Solarus Quest Editor proposera l'export direct de votre quête vers la Solatrix :</span></div>
&nbsp;

<a href="../data/fr/entities/article/old/2016/04/images/editor_screenshot.png"><img class="aligncenter size-medium wp-image-38031" src="/images/editor_screenshot-277x300.png" alt="Export depuis Solarus Quest Editor vers la Solatrix" width="277" height="300" /></a>

Le Pack Mercuris vous proposera la console accompagnée de Zelda Mercuris' Chest, notre vaporware phare.

<a href="../data/fr/entities/article/old/2016/04/images/console2.jpg"><img class="aligncenter wp-image-38027 size-medium" src="/images/console2-225x300.jpg" alt="console2" width="225" height="300" /></a>

Compatible avec les manettes les plus populaires du marché, la Solatrix vous promet des moments de jeu plus intenses que jamais.

<a href="../data/fr/entities/article/old/2016/04/images/console3.jpg"><img class="aligncenter wp-image-38028 size-medium" src="/images/console3-300x225.jpg" alt="console3" width="300" height="225" /></a>

<span class="author-a-wymz79zlmz73zz86z09z70zz72z22z122zm">Toute l'équipe de Solarus Software a travaillé d'arrache-pied pour donner vie à ce projet ! Les plans seront bientôt disponibles afin que vous puissiez vous aussi construire votre Solatrix.</span>

<a href="../data/fr/entities/article/old/2016/04/images/console4.jpg"><img class="aligncenter wp-image-38029 size-medium" src="/images/console4-300x225.jpg" alt="console4" width="300" height="225" /></a>

On se retrouve dès l'année prochaine pour une nouvelle mise à jour. Voilà en tout cas de belles heures de jeu et de joie en perspective !
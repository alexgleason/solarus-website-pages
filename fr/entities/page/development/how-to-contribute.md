[container]
[row]
[column width="8"]

# {title}

Vous voulez participer au projet Solarus ? Toute la communauté vous remercie et toute aide, même très petite, est la bienvenue. Venez parler avec nous sur [Discord](https://discord.gg/PtwrEgZ) pour nous dire ce que vous voulez apporter au projet.

## Développement

Tous le source code des projets Solarus se trouve sur Gitlab. Le moteur est fait en C++. Il est recommandé d'être compétent dans ce langage et de maîtriser la programmation de jeu vidéo avant d'être capable de contribuer. L'éditeur de quête Solarus Quest Editor est fait en C++ avec Qt.

* [Projets Solarus sur Gitlab](https://gitlab.com/solarus-games)
* [Documentation du code C++ du moteur](http://www.solarus-games.org/developer_doc/latest)

Si vous voulez nous **informer d'un bug**, ou si vous avez une **demande de nouvelle fonctionnalité**:

1. Regardez si quelqu'un n'a pas déjà créé un ticket sur la page Gitlab du projet.
2. Créez votre ticket, et soyez aussi précis que possible. Joignez des captures d'écran si besoin.

Si vous voulez **modifier le code source** de n'importe quel projet :

1. Clonez le dépôt pour lequel vous voulez contribuer.
2. Créez votre propre branche et faites vos modifications dans cette branche.
3. Créez une Merge Request.

## Tutoriels pour créateurs de quêtes

Vous savez déjà très bien comment créer une quête Solarus ? Alors pourquoi pas aider les autres à apprendre aussi ? Des tutoriels ont besoin d'être écrits pour aider les débutants et les créateurs de quête plus expérimentés. Nous avons un dépôt sur Gitlab pour les tutoriels officiels. Ceux-ci sont écrits en markdown, dont la syntaxe est très simple.

* [Tutoriels Solarus sur Gitlab](https://gitlab.com/solarus-games/learn-solarus)

Suivez les mêmes étapes qu'un développeur si vous voulez contribuer aux tutoriels :

1. Clonez le dépôt pour lequel vous voulez contribuer.
2. Créez votre propre branche et faites vos modifications dans cette branche.
3. Créez une Merge Request.

## Art

Si vous avez la fibre artistique, nous avons besoin d'aide dans le département pixel-art. Toute contribution de tileset, font ou sprite sont les bienvenues ! Vous voulez peut-être également aider pour le design du site Internet ?

* [Éléments de design graphqiue pour Solarus sur Gitlab](https://gitlab.com/solarus-games/solarus-design)
* [Pack de ressources libres pour Solarus sur Gitlab](https://gitlab.com/solarus-games/solarus-free-resource-pack)

## Traductions

L'éditeur de quête et le lanceur ont besoin d'être traduits dans tous les langages possibles ! N'hésitez pas à ajouter votre propre langage. Puisqu'ils sont faits avec Qt, vous devriez utiliser le logiciel de Qt fait pour cela, appelé Qt Linguist.

Les jeux peuvent également bénéficier de nouvelles traductions. Solarus Quest Editor inclut un système de traduction pour vous simplifier le travail.

* [Tutoriel sur la traduction de quête](tutorials)

## Dons

Les développeurs de Solarus réalisent ce moteur sur leur temps libre.
Si vous appréciez notre travail, vos dons pourront contribuer aux frais d'héberger et nous montrer que Solarus est apprécié.

* [Faire un don](donation)

[/column]

[column width ="4"]
[summary level-min="2" level-max="3"]
[/column]

[/row]
[/container]
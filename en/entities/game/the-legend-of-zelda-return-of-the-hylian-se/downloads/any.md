### Download Solarus

To play a Solarus quest, you need the **Solarus Launcher** application. Please make sure you have [installed it first](/en/solarus/download).

If you already have Solarus Launcher, you can skip to **step 2**.

### Download the quest

Click on the following button to download the quest as a **.solarus file**. You can then open it from **Solarus Launcher**.

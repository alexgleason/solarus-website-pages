[container layout="small"]

# {title}

## Why donate

The Solarus engine, Solarus Quest Editor and all our games are free and open-source. We spend a lot of time working on the projects, and we intend to continue!

If you appreciate our work, your donations can help with the hosting fees and show that Solarus is appreciated. You can donate any amount you want via the following ways. Your support is very appreciated!

## How to donate

[streamlabs title="Streamlabs" label="Donate" icon="donate" link="https://streamlabs.com/christophogames"]

This is the preferred way for a donation. It goes to Christopho, the creator of Solarus. He often streams himself coding the engine, games, or even let's plays for fun.

[/streamlabs]

[paypal title="Paypal" email="christopho128@gmail.com" name="Christopho" label="Donate" icon="paypal" icon-category="fab" subject="Solarus"]

You don't need to have a paypal account in order to make a donation.

[/paypal]

[bitcoin title="Bitcoin" image="/data/assets/images/bitcoin_qrcode.png" icon="bitcoin" icon-category="fab" label="Donate" link="bitcoin:1MdMLRPFZf3tq3CqWMjoLqfY1enjhzJqVg"]

Use the following address or the following QR-Code:
[align type="center"]

#### [`1DyA88zEr1P2phrMWm76QNxc82RRC3nR95`](bitcoin:1MdMLRPFZf3tq3CqWMjoLqfY1enjhzJqVg)

[![Bitcoin QR Code](../../../../assets/images/bitcoin_qrcode.png)](bitcoin:1DyA88zEr1P2phrMWm76QNxc82RRC3nR95)

[/align]

[/bitcoin]

[/container]

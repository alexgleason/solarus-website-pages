[container layout="small"]

# {title}

## Pourquoi faire un don

Le moteur Solarus, l'éditeur Solarus Quest Editor et tous nos jeux sont libres et open-source. Nous passons beaucoup de temps sur ces projets, et nous n'avons pas l'intention d'arrêter !

Si vous appréciez notre travail, vos dons peuvent aider à payer les coûts de l'hébergement et montrer que Solarus est apprécié. Vous pouvez faire un don du montant que vous voulez via les moyens suivants. Votre support fait extrêmement plaisir !

## Comment faire un don

[streamlabs title="Streamlabs" label="Faire un don" icon="donate" link="https://streamlabs.com/christophogames"]

C'est le moyen de préférence pour faire un don. L'argent va à Christopho, le créateur de Solarus. Il diffuse souvent en direct des sessions de développement où il se filme pendant qu'il code le moteur et les jeux (et parfois mêmes des let's play pour le fun).

[/streamlabs]

[paypal title="Paypal" email="christopho128@gmail.com" name="Christopho" label="Faire un don" icon="paypal" icon-category="fab" subject="Solarus"]

Vous n'avez pas besoin d'avoir un compte Paypal pour faire un don.

[/paypal]

[bitcoin title="Bitcoin" image="/data/assets/images/bitcoin_qrcode.png" icon="bitcoin" icon-category="fab" label="Faire un don" link="bitcoin:1MdMLRPFZf3tq3CqWMjoLqfY1enjhzJqVg"]

Utilisez l'adresse ou le QR-Code suivants:
[align type="center"]

#### [`1DyA88zEr1P2phrMWm76QNxc82RRC3nR95`](bitcoin:1MdMLRPFZf3tq3CqWMjoLqfY1enjhzJqVg)

[![Bitcoin QR Code](../../../../assets/images/bitcoin_qrcode.png)](bitcoin:1DyA88zEr1P2phrMWm76QNxc82RRC3nR95)

[/align]

[/bitcoin]

[/container]

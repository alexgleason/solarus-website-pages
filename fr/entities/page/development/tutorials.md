[container]

# {title}

[alert type="secondary" title="Heye!" icon="exclamation-triangle"]
Cette page est actuellement en construction.
[/alert]

## Tutoriels vidéos

Christopho a réalisé des tutoriels vidéos sur Youtube traitant des bases de Solarus. Ils couvrent la création d'un ARPG avec le moteur et l'éditeur. Vous pouvez également aller voir la [playlist entière](https://www.youtube.com/playlist?list=PLzJ4jb-Y0ufzB4nXkSINFhbrtLlnlI4li).

1. [Créer une quête](https://www.youtube.com/watch?v=rsammSHv7xc)
2. [Pack de ressources Zelda](https://www.youtube.com/watch?v=29poVVE7z_E)
3. [L'éditeur de maps](https://www.youtube.com/watch?v=6Rt8kXxYgZs)
4. [Les terrains](https://www.youtube.com/watch?v=0WjsKNCZDA8)
5. [L'arbre de la quête](https://www.youtube.com/watch?v=B0ae_lJpsLg)
6. [Les propriétés de la quête](https://www.youtube.com/watch?v=AHzQOqteimM)
7. [Les bases des scripts Lua](https://www.youtube.com/watch?v=dXqJLRJQVW8)
8. [Les trésors](https://www.youtube.com/watch?v=8pJ9nedg9Qw)
9. [La sauvegarde](https://www.youtube.com/watch?v=P45FQDdUsFg)
10. [La téléportation](https://www.youtube.com/watch?v=9YyEpN0z1OY)
11. [Comment scroller entre maps de tailles différentes](https://www.youtube.com/watch?v=5eoK88SyYws)
12. [Escaliers et trous](https://www.youtube.com/watch?v=UNwFFsr1V-Y)
13. [Séparez le code en plusieurs fichiers avec require()](https://www.youtube.com/watch?v=3ub9aTFq8LA)
14. [Comment créer un tileset](https://www.youtube.com/watch?v=DGaunR-3em4)
15. [Les scripts de map](https://www.youtube.com/watch?v=t-ywwnSbGQA)
16. [Comment créer un sprite](https://www.youtube.com/watch?v=9kUsFuqtzSo)
17. [Afficher une image](https://www.youtube.com/watch?v=8g-JIVNb3g8)
18. [Dialoguer avec un personnage non-joueur](https://www.youtube.com/watch?v=Qvc2cQzuL-E)
19. [Les PNJ généralisés](https://www.youtube.com/watch?v=rte8Yd5OFvk)
20. [Les mouvements](https://www.youtube.com/watch?v=PkGl6SG5JJc)
21. [Les sauteurs](https://www.youtube.com/watch?v=fyQAQzhAzZQ)
22. [Les plates-formes en demi-niveau](https://www.youtube.com/watch?v=vvQEM0NfZCs)
23. [Interrupteur qui déclenche un mécanisme](https://www.youtube.com/watch?v=v0nK8GoNJNg)
24. [Les tiles dynamiques](https://www.youtube.com/watch?v=J2OssGIa828)
25. [Les timers](https://www.youtube.com/watch?v=9mJzkEl6YaE)
26. [Les ennemis](https://www.youtube.com/watch?v=h3g2PAyUl6Q)
27. [Les blocs](https://www.youtube.com/watch?v=KOaKe5G3LbA)
28. [Les murs invisibles](https://www.youtube.com/watch?v=x61rZCI32L8)
29. [Les séparateurs](https://www.youtube.com/watch?v=Yj-dMFqiF98)
30. [Les capteurs](https://www.youtube.com/watch?v=GCixuthVxtU)
31. [Les objets destructibles](https://www.youtube.com/watch?v=D7mqAscMpK8)
32. [Les portes](https://www.youtube.com/watch?v=0BLePicALSo)
33. [Le HUD](https://www.youtube.com/watch?v=Y0fzDSgk7SI)
34. [Les Rubis](https://www.youtube.com/watch?v=ROfzXEj-cqs)
35. [Les magasins](https://www.youtube.com/watch?v=gnR23NLR4Pc)
36. [La nage](https://www.youtube.com/watch?v=KzblUhVng3s)

## Guide de migration

Si votre jeu utilise toujours une ancienne version de Solarus, il se peut que vous vouliez migrer vers une plus récente. Nous avons rédigé un [guide de migration](/en/development/tutorials/migration-guide) (uniquement en Anglais).

[/container]

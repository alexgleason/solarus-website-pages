And... Solarus 1.0.4 is out!

Two crashes in ZSXD remained despite all my tests, and the customization screen of joypad commands was also broken.
<ul>
	<li><a title="Download Solarus" href="http://www.solarus-games.org/downloads/download-solarus/">Download Solarus 1.0.4</a></li>
	<li><a title="Download ZSDX" href="http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-dx/">Download Zelda Mystery of Solarus DX 1.6.2</a></li>
	<li><a title="Download ZSXD" href="http://www.solarus-games.org/download/download-zelda-mystery-of-solarus-xd/">Download Zelda Mystery of Solarus XD 1.6.2</a></li>
</ul>
<h2>Changes in Solarus 1.0.4</h2>
<ul>
	<li>Don't die if a script tries so show a missing string (#237).</li>
	<li>Don't die if a treasure has a variant unexistent in the item sprite.</li>
	<li>Fix customization of joypad commands.</li>
</ul>
<div>
<h2>Changes in Zelda Mystery of Solarus DX 1.6.2</h2>
<div>
<ul>
	<li> Fix two line errors in English dialogs.</li>
</ul>
</div>
</div>
<h2>Changes in Zelda Mystery of Solarus XD 1.6.2</h2>
<ul>
	<li>Fix a crash due to a missing text in the French options pause menu.</li>
	<li>Fix a crash due to an invalid treasure in a pot of crazy house 1F.</li>
	<li>Fix a typo in the French inventory pause menu.</li>
</ul>
Please continue to report any bugs. I expect more bugs to be found because this upgrade of both quest to Solarus 1.0 is really a heavy change for them.
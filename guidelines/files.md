# Files

Markdown and JSON files respect some rules that we want everyone to follow.

A Markdown and its associate JSON file always have the same name, apart from the file extension. They are always located both in the same folder.

```
my-page-name.md
my-page-name.json
```

## Markdown files

Markdown files are basically the page content: titles, subtitles, paragraphs, some images, etc. For instance, the news for the blog are written in Markdown. Markdown does not allow fancy layouts, but it is sufficient for basic page structure like news articles or tutorials.

### Shortcodes

However, some pages need more design work on the layout. This is why we expanded Markdown with some shortcodes.

#### How to use

Shortcodes are defined with an opening tag and a closing tag. They can have parameters.

```php
[my-shortcode param1="value1" param2="value2"]
// Content
[/my-shortcode]
```

Shortcodes can be nested if necessary. Please check in your browser each time you nest shortcodes to ensure there is no resulting layout issues. If so, create an issue.

#### Where to use

We don't want to use shortcodes everywhere because this is not pure markdown. Here is a summary of where we allow use of shortcodes (marked with a checkmark ✓) and where we don't (no checkmark).

|Page                       |Shortcodes allowed |
|---------------------------|-------------------|
|`downloads/*`              |                   |
|`entities/article/*`       |                   |
|`entities/game/*`          |                   |
|`entities/page/*`          |✓                  |
|`entities/resource-pack/*` |                   |
|`errors/*`                 |✓                  |
|`models/*`                 |✓                  |
|`widgets/*`                |✓                  |

#### Shortcodes list

Here is the list of available shortcodes. For more detailed specifications, please refer to the documentation.

|Name           |Function                                           |
|---------------|---------------------------------------------------|
|`alert`        |Displays a message as an alert.                    |
|`badge`        |Displays a small badge.                            |
|`button`       |Displays a button.                                 |
|`column`       |Specifies a new container column.                  |
|`container`    |Specifies a new container grid.                    |
|`cover`        |Specifies a new container cover.                   |
|`icon`         |Displays an icon from Fontawesome.                 |
|`jumbotron`    |Displays a jumbotron.                              |
|`model`        |Displays and translates a model.                   |
|`paypal`       |Displays a donate button with paypal.              |
|`progressbar`  |Displays a progress bar.                           |
|`row`          |Specifies a new row grid.                          |
|`space`        |Displays an empty space.                           |
|`summary`      |Displays a summary of the page's titles hierarchy. |
|`table`        |Specifies a new container table.                   |
|`table-row`    |Specifies a new row in the table.                  |
|`table-column` |Specifies a new column in the table.               |
|`youtube`      |Displays a youtube video.                          |

### Variables

You may use variables in your Markdown file. These variables are specified in the JSON file. The variable name is the key, and the variable value will be the associated value. Consequently, variables are limited to the page's properties: `title`, `slug`, etc.).

They are written between curly brackets. The variable will be automatically replaced by its actual value when the page will be parsed by the engine.

```php
`{my-variable}`
```

## JSON

JSON files specify the properties of the page: name, creation-date, slug, etc.). Some are mandatory, and some aren't. All these properties have previously been declared in the entity structure, in the `structure.md` file at the root of the entity folder.

For instance, here is the JSON file for the contribution page:

```json
{
  "has_breadcrumb": true,
  "meta_description": "How to contribute to the Solarus game engine project.",
  "meta_title": "How to contribute",
  "parent": "development",
  "publish": true,
  "slug": "development/how-to-contribute",
  "title": "How to contribute"
}
```

* The `parent` is the parent of the page that will be dislayed in the breadcrumb.
* The `publish` property specifies if the page will be visible (`true`) or not (`false`).
* The `slug` property is the address of the page.
* etc.

Some pages have more complex JSON files, e.g. the games. JSON also allow for nested tables and lists. See following example:

```json
{
  "controls": ["keyboard", "gamepad"],
  "medias": [
    {
      "id": "BUxREyXILLs",
      "thumbnail": "medias/screen_1.png",
      "type": "youtube"
    },
    {
      "image": "medias/screen_2.png",
      "thumbnail": "medias/screen_2.png",
      "type": "image"
    }
  ]
}
```

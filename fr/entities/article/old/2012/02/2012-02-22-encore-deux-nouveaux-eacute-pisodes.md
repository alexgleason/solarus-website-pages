Deux nouveaux épisodes de la soluce vidéo de [url=http://www.zelda-solarus.com/jeu-zsdx]Zelda Mystery of Solarus DX[/url] sont disponibles depuis ce matin !

[list]
[li] [url=http://www.youtube.com/watch?v=b5nG2ySbX1w]Épisode 5 : du niveau 2 au niveau 3[/url][/li]
[li] [url=http://www.youtube.com/watch?v=nr_KzjsMw4o]Épisode 6 : niveau 3 - Antre de Maître Arbror[/url][/li]
[/list]

Et c'est avec Thyb que j'ai eu le plaisir de commenter ces deux épisodes. La soluce va donc maintenant jusqu'à la fin du troisième donjon. Le tournage de la suite est prévu pour jeudi soir ;)